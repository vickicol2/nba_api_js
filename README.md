## Ejercicio mínimo

- Buscar una API gratuita 

Ejs.

https://reqres.in/

https://swapi.dev/

https://developer.marvel.com/

https://pokeapi.co/

https://rickandmortyapi.com/

- Hacer una petición a la API para recibir un listado de elementos (personajes, artículos, vehículos, etc.). Esto os tiene que devolver un JSON.

- Recorrer este JSON y para cada elemento representarlo en mi web.

## Extras

- En vez de tenerlos en un JSON cargarlos en una estructura de clases que hayamos creado y que tenga los métodos para representar.

- Hacer un buscador que filtre un algún criterio.

- Al pinchar en un elemento de una lista, nos llevará a los detalles de ese elemento.

## La API elegida para el proyecto: https://www.balldontlie.io/#introduction API de la NBA, además debe instalarse la extensión Moesif Orign & CORS Changer en google https://chrome.google.com/webstore/detail/moesif-orign-cors-changer/digfbfaphojjndkpccljibejjbppifbc/related?hl=en-US para poder permitir el CROSS ORIGIN y hacer las consultas a la API.