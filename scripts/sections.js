 
 function createHeader(){
  const header = document.getElementsByClassName('header')[0];
  header.appendChild(DOM.createElement('div', undefined, ['header__container']));

  const container = document.getElementsByClassName('header__container')[0];
  container.appendChild(DOM.createElement('div', undefined, ['header__content']));

  const content = document.getElementsByClassName('header__content')[0];
  content.appendChild(DOM.createElement('div', undefined, ['header__imagen']));
  content.appendChild(DOM.createElement('div', undefined, ['header__nav']));

  const content_img = document.getElementsByClassName('header__imagen')[0];
  content_img.appendChild(DOM.createElement('img', undefined, ['header__logo'], [{name:'src', value:'/assets/logo.svg'}, {name:'alt', value:'Logo NBA'}]));

  const content_nav = document.getElementsByClassName('header__nav')[0];
  content_nav.appendChild(DOM.createElement('nav', undefined ));

  const nav = document.getElementsByTagName('nav')[0];
  nav.appendChild(DOM.createNavComponent(3));

  const liTeams = document.getElementsByClassName('header__link')[0];
  liTeams.setAttribute('href', "#Teams");
  liTeams.setAttribute('id', "teams");
  liTeams.appendChild(document.createTextNode('Equipos'));

  const liPlayers = document.getElementsByClassName('header__link')[1];
  liPlayers.setAttribute('href', "#Players");
  liPlayers.setAttribute('id', "players");
  liPlayers.appendChild(document.createTextNode('Jugadores'));

  const liGames = document.getElementsByClassName('header__link')[2];
  liGames.setAttribute('href', "#Games");
  liGames.setAttribute('id', "games");
  liGames.appendChild(document.createTextNode('Resultados'));
}

 function createSections(){
    const main = document.getElementsByTagName('main')[0];
    main.appendChild(DOM.createElement('section', undefined, ['section__teams'], [{name:'id', value:'Teams'}]));
    main.appendChild(DOM.createElement('section', undefined, ['section__players'], [{name:'id', value:'Players'}]));
    main.appendChild(DOM.createElement('section', undefined, ['section__games'], [{name:'id', value:'Games'}]));
    headerTeams();
    headerPlayers();
    headerGames();

    const sectionPlayers = document.getElementsByClassName('section__players')[0];
    sectionPlayers.appendChild(DOM.createElement('div', undefined, ['players__buttons']));
    const contentButtons = document.getElementsByClassName('players__buttons')[0];
    contentButtons.appendChild(DOM.createElement('button', undefined, ['players__buttons__actions', 'fas', 'fa-caret-left'], [{name:'id', value: 'back'}]));
    contentButtons.appendChild(DOM.createElement('button', undefined, ['players__buttons__actions', 'fas', 'fa-caret-right'], [{name:'id', value: 'next'}]));
    
    const sectionGames = document.getElementsByClassName('section__games')[0];
    sectionGames.appendChild(DOM.createElement('div', undefined, ['games__actions']));
    const DivActions = document.getElementsByClassName('games__actions')[0];
    DivActions.appendChild(DOM.createElement('select', undefined, ['games__actions__select'], [{name: 'id', value: 'selectTeam'}, {name: 'name', value:'select'}]));
    DivActions.appendChild(DOM.createElement('button', "Temporada 2018-2019", ['games__actions__buttons'], [{name:'id', value: 'temp18'}]));
    DivActions.appendChild(DOM.createElement('button', "Temporada 2017-2018", ['games__actions__buttons'], [{name:'id', value: 'temp17'}]));
   
    // Carga todas las opciones del select con los nombre de los equipos
    selectAllTeams(); 
}


 function headerTeams(){
   const sectionTeams = document.getElementsByClassName('section__teams')[0];
   sectionTeams.appendChild(DOM.createElement('div', undefined, ['teams__header']));

   const header = document.getElementsByClassName('teams__header')[0];
   header.appendChild(DOM.createElement('h2', "Equipos", ['teams__header__h2']));
   header.appendChild(DOM.createElement('p', "Los equipos de la NBA se clasifican en divisiones, según la zona donde juegan y a su vez en conferencias. ¡Descubre los equipos que hay en la liga de baloncesto de la NBA!", ['teams__header__info']));
   header.appendChild(DOM.createElement('div', undefined, ['teams__actions']));

   const buttons = document.getElementsByClassName('teams__actions')[0];
   buttons.appendChild(DOM.createElement('button', "Todos", ['teams__actions__button'], [{name: 'id', value: 'teamsAll'}]));
   buttons.appendChild(DOM.createElement('button', "Conferencia Oeste", ['teams__actions__button'], [{name: 'id', value: 'teamsWest'}]));
   buttons.appendChild(DOM.createElement('button', "Conferencia Este", ['teams__actions__button'], [{name: 'id', value: 'teamsEast'}]));
}

 function headerPlayers(){
  const sectionPlayers = document.getElementsByClassName('section__players')[0];
  sectionPlayers.appendChild(DOM.createElement('div', undefined, ['players__header']));

  const header = document.getElementsByClassName('players__header')[0];
  header.appendChild(DOM.createElement('h2', "Jugadores", ['players__header__h2']));
  header.appendChild(DOM.createElement('p', "¡Conoce a los protagonistas de tus equipos favoritos!", ['players__header__info']));
  header.appendChild(DOM.createElement('input', undefined, ['players__header__input'], [{name: 'type', value:'text'}, {name: 'placeholder', value:'Busca tu jugador'}, {name: 'id', value: 'searchPlayer'}, {name:'pattern', value: '[A-Za-z]'}]));
}

function headerGames(){
  const sectionPlayers = document.getElementsByClassName('section__games')[0];
  sectionPlayers.appendChild(DOM.createElement('div', undefined, ['games__header']));

  const header = document.getElementsByClassName('games__header')[0];
  header.appendChild(DOM.createElement('h2', "Resultados de los juegos", ['games__header__h2']));
}

// Crea la lista desplegable con todos los equipos
async function selectAllTeams(){
  const selectTeam = document.getElementById('selectTeam');
  const data = await DAO.getIdTeam(); 

  data.forEach(val => {
    let id = val.id.toString();
    selectTeam.appendChild(DOM.createElement('option', val.full_name, undefined, [{name: 'value', value: id}]));
  });

}

function createBtonUp(){
  const body = document.getElementsByTagName('body')[0];
  body.appendChild( DOM.createElement('span', undefined, ['button__up', 'fas', 'fa-angle-double-up'], [{name:'id', value: 'btonUp'}]));
}

function createFooter(){
  const footer = document.getElementsByClassName('footer')[0];
  footer.appendChild(DOM.createElement('div', undefined, ['footer__container']));
  footer.appendChild(DOM.createElement('div', undefined, ['footer__container']));

  const footerCopyright = document.getElementsByClassName('footer__container')[0];
  footerCopyright.appendChild(DOM.createElement('span', undefined, ['far', 'fa-copyright']));
  footerCopyright.appendChild(DOM.createElement('p', 'Maria Victoria Colmenares 2020'));
  footerCopyright.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-instagram'], [{name: 'href', value: 'https://www.instagram.com/vickicolmenares/'}, {name: 'target', value: '_blank'}]));
  footerCopyright.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-linkedin'], [{name: 'href', value: 'https://www.linkedin.com/in/maria-victoria-colmenares-castro/'}, {name: 'target', value: '_blank'}]));
  footerCopyright.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-gitlab'], [{name: 'href', value: 'https://gitlab.com/vickicol2'}, {name: 'target', value: '_blank'}]));

  const footerNba = document.getElementsByClassName('footer__container')[1];
  footerNba.appendChild(DOM.createElement('p', 'Conoce más sobre la NBA'));
  footerNba.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-instagram'], [{name: 'href', value: 'https://www.instagram.com/nba/'}, {name: 'target', value: '_blank'}]));
  footerNba.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-twitter'], [{name: 'href', value: 'https://twitter.com/nba?lang=es'}, {name: 'target', value: '_blank'}]));
  footerNba.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-facebook'], [{name: 'href', value: 'https://www.facebook.com/nba/'}, {name: 'target', value: '_blank'}]));
  footerNba.appendChild(DOM.createElement('a', undefined, ['footer__icon', 'footer__icon__link', 'fab', 'fa-youtube'], [{name: 'href', value: 'https://www.youtube.com/user/NBA'}, {name: 'target', value: '_blank'}]));
}



