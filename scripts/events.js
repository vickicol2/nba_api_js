var posicionActual = 0;
var totalPlayers = 10;


// Para pasar a la siguiente tarjeta del jugador, en el carousel de imágenes
function nextPlayer(){
    const player = document.getElementById('searchPlayer').value; // input
    (posicionActual == totalPlayers)? posicionActual = 0 : posicionActual++;
    DAO.getPlayers(posicionActual, player);
}


function backPlayer(){ 
    const player = document.getElementById('searchPlayer').value;  // input
    (posicionActual <= 0)? posicionActual = totalPlayers : posicionActual--;
    DAO.getPlayers(posicionActual, player);
}


// Toma el valor del input y con eso busca a los jugadores
async function searchPlayer(){
    const player = document.getElementById('searchPlayer').value; //input
    const data = await DAO.getTotalPlayers(player); //obtiene el json.data
    totalPlayers = data.length - 1;
    posicionActual = 0;
    DAO.getPlayers(posicionActual, player);
}

// Toma el id del seleccionado en el select y al presionar el botón temporada 2018-2019 arroja los juegos de ese equipo en esa fecha
async function gamesTeam18(){
    const id = document.getElementById('selectTeam').value;
    var route = `/games?seasons[]=2018&team_ids[]=${id}`;
    DAO.getGames(route);
}

async function gamesTeam17(){
    const id = document.getElementById('selectTeam').value;
    var route = `/games?seasons[]=2017&team_ids[]=${id}`;
    DAO.getGames(route);
}

// Botón para ir al inicio
function scrollUp(){
    const currentScroll = document.documentElement.scrollTop;
    if (currentScroll > 0){
        window.scrollTo(0,0);
    }
}
