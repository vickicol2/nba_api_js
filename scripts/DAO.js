/**
 * Clase de utilizades relacionadas con los datos
 * Data access object (DAO), capa de acceso a datos
 * Consulta a una API
 */
class DAO {

    static baseURL = "https://balldontlie.io/api/v1";

    /**
     * Método que devuelve un Array con los todos jugadores de la NBA
     * @returns Array Get Player NBA
     */
    static getTeams(conferencia) {
      const route = "/teams";

      fetch(DAO.baseURL + route)
      .then(response => response.json())
      .then(function(teams){  
        DOM.createCardsTeams(15,teams, conferencia); // 15 para limitar el total de jugadores
      });
    }

    
    /**
     * Devuelve todos los jugadores, con determinado criterio de búsqueda mediante el parámetro player.
     * @param {int} posicion 
     * @param {string} player 
     */
    static getPlayers(posicion, player) {
      const route = `/players?search=${player}`;

      fetch(DAO.baseURL + route)
      .then(response => response.json())
      .then(function(players){ 
        DOM.createCardPlayer(posicion, players);
      });
    }


    /**
     * Devuelve el total de jugadores, que cumplen un criterio de búsqueda, mediante el parámetro player
     * @param {string} player 
     */
    static async getTotalPlayers(player) {
      const route = `/players?search=${player}`;

        try {
          const response = await fetch(DAO.baseURL + route);
          const json = await response.json();
          return json.data;
        } catch (err) {
          console.log(err);
          throw err;
        }
    }


    /**
     * Devuelve el listado de todos los juegos por la temporada indicada en route
     * @param {string} route 
     */
    static getGames(route) {
      fetch(DAO.baseURL + route)
      .then(response => response.json())
      .then(function(games){  
        DOM.createGames(10,games);
      });
    }


    /**
     * Devuelve los equipos para luego buscar su id (llamado desde section.js para cargar los options del select: selectAllTeams())
     */
    static async getIdTeam() {
      const route = "/teams";

        try {
          const response = await fetch(DAO.baseURL + route);
          const json = await response.json();
          return json.data;
        } catch (err) {
          console.log(err);
          throw err;
        }
    }


}