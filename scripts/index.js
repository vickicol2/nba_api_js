
var addListeners = () => {
    document.getElementById('teamsAll').addEventListener('click', function(){ DAO.getTeams("all"); });
    document.getElementById('teamsWest').addEventListener('click', function(){ DAO.getTeams("West");});
    document.getElementById('teamsEast').addEventListener('click', function(){ DAO.getTeams("East");});
    document.getElementById('temp18').addEventListener('click', gamesTeam18);
    document.getElementById('temp17').addEventListener('click', gamesTeam17);
    document.getElementById('next').addEventListener('click', nextPlayer);
    document.getElementById('back').addEventListener('click', backPlayer);
    document.getElementById('searchPlayer').addEventListener('keyup', searchPlayer);
    document.getElementById('btonUp').addEventListener('click', scrollUp);
}

window.onload = () => {
    createHeader();
    createSections();
    createBtonUp();
    createFooter();
    DAO.getTeams("all");
    DAO.getPlayers(0, '');
    addListeners();
};


window.onscroll = () => {
    // El método Window.scroll() desplaza la ventana a un lugar particular en el documento.
    // window.scroll(x-coord, y-coord)
    // Para ocultar y mostrar el botón.
    const currentScroll = document.documentElement.scrollTop;
    const btonUp = document.getElementById('btonUp');
    (currentScroll > 100)? btonUp.style.transform = "scale(1)" : btonUp.style.transform = "scale(0)";
}
