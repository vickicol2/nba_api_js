class DOM {

    /**
     * Create un elemento del DOM. Ejemplo:
     * DOM.createElement('div', "mi contenido o undefined", ['clase1, 'clase2', 'clase3'], [{name: 'data', value: 'custom'},{name: 'rol', value: 'button'},])
     * 
     * @param {String} type 
     * @param {String} txtContent 
     * @param {Array} classes 
     * @param {Array} attributes
     * @returns {HTMLElement}
     */
    static createElement(type, txtContent, classes, attributes) {

        if (typeof type !== "string") {
            const err = new Error();
            err.message = 'No se ha definido el tipo de elemento a crear.';
            err.name = "Error de parámetros";
            throw err;  // Lanza el error y no ejecuta nada más de esta función
        }

        const element = document.createElement(type);

        if (typeof txtContent === "string") {
            const txt = document.createTextNode(txtContent);
            element.appendChild(txt);
        }

        if (Array.isArray(classes)) {
            //element.classList.add(...classes); //spread
            for (let thisClass of classes) {
                element.classList.add(thisClass);
            }
        }
        
        if (Array.isArray(attributes)) {
            for (let attribute of attributes) {

                if (typeof attribute.name === "string" && typeof attribute.value === "string") {
                    element.setAttribute(attribute.name, attribute.value);
                }
                else {
                    const err = new Error();
                    err.message = 'El array de atributos tiene que tener pares {name: value} y ser string';
                    err.name = "Error de parámetros";
                    throw err;
                }
            }
        }

        return element;
    }


    /**
     * Crea la barra de navegación
     * @param {int} number 
     */
    static createNavComponent(number) {
        const ul = DOM.createElement('ul', undefined, ['header__ul']);

        for (let i = 0; i < number; i++) {
            const li = DOM.createElement('li');
            const a = DOM.createElement('a', undefined, ['header__link']);
            li.appendChild(a);
            ul.appendChild(li);
        }

        return ul;
    } 


    /**
     * Crea las tarjetas de los equipos.
     * @param {int} numberTeams 
     * @param {Array} teams 
     * @param {String} conferencia 
     */
    static createCardsTeams(numberTeams, teams, conferencia){
        const sectionTeams = document.getElementsByClassName('section__teams')[0];
        var card = document.getElementsByClassName('card')[0];

        if (card != undefined){  // Si el div contenedor de las tarjetas de equipo existen, lo eliminamos para crear nuevas tarjetas, porque sino por cada petición se generan tarjetas sin eliminar las anteriores
            const padre = card.parentNode;  // Obtenemos el padre de card (section__teams)
            padre.removeChild(card);        // Con el padre eliminamos el hijo card (contiene todos los equipos)
            card = DOM.createElement('div', undefined, ['card'], [{name: 'id', value: 'card'}]);
        } else {
            // Si no existe creamos el contenedor de las tarjetas
            card = DOM.createElement('div', undefined, ['card'], [{name: 'id', value: 'card'}]);
        }
        
   
        for (let i = 0; i< numberTeams; i++){
            if (teams.data[i].conference == conferencia || conferencia == "all"){
                // console.log(`conferencia en el if: ${conferencia}`);
                const container = DOM.createElement('div', undefined, ['card__container']);
                card.appendChild(container);
    
                const containerFaces = DOM.createElement('div', undefined, ['card__container__faces']);
                container.appendChild(containerFaces);
                
                // ------ Card_front -----
                const cardFront = DOM.createElement('div', undefined, ['card__front']);
                containerFaces.appendChild(cardFront);
                
                cardFront.appendChild(DOM.createElement('img', undefined, ['card__front__img'], [{name:'src', value: `/assets/${teams.data[i].abbreviation}.png`}, {name:'alt', value: teams.data[i].abbreviation}]));
                cardFront.appendChild(DOM.createElement('h2', teams.data[i].abbreviation, ['card__front__h2']));
    
                // ------ Card_back -------
                
                const cardBack = DOM.createElement('div', undefined, ['card__back']);
                containerFaces.appendChild(cardBack);
    
                cardBack.appendChild(DOM.createElement('p', `${teams.data[i].full_name} (${teams.data[i].name})` , ['card__back__p']));
                cardBack.appendChild(DOM.createElement('p', `Abreviación: ${teams.data[i].abbreviation}`, ['card__back__p']));
                cardBack.appendChild(DOM.createElement('p', `Ciudad: ${teams.data[i].city}`, ['card__back__p']));
                cardBack.appendChild(DOM.createElement('p', `Conferencia: ${teams.data[i].conference}`, ['card__back__p']));
                cardBack.appendChild(DOM.createElement('p', `División: ${teams.data[i].division}`, ['card__back__p']));
            }
            
        }
        sectionTeams.appendChild(card);
    }


    /**
     * Crea la tarjeta del jugador
     * @param {int} posicion 
     * @param {Array} players 
     */
    static createCardPlayer(posicion, players){
        if (players.data.length > 0){
            const sectionPlayers = document.getElementsByClassName('section__players')[0];
            var carousel = document.getElementsByClassName('carousel')[0];
            var info = document.getElementsByClassName('carousel__data__info')[0];

            // Si el contenedor principal (carousel) existe -> se elimina por medio del padre, y luego se vuelve a crear
            // Si es undefined (no está creado) -> se crea
            // Esto es para que cada búsqueda de jugadores se elimine la tarjeta, y se cree una nueva con los datos nuevos (porque sino se irán creando tarjetas debajo de otra por cada búsqueda)

            if (carousel != undefined){
                const padre = carousel.parentNode;
                padre.removeChild(carousel);
                carousel = DOM.createElement('div', undefined, ['carousel']);
                sectionPlayers.appendChild(carousel);
            } else {
                carousel = DOM.createElement('div', undefined, ['carousel']);
                sectionPlayers.appendChild(carousel);
            }

            // Igual lo hacemos con el párrafo que tendrá el texto No hay jugadores
            // Si existe -> lo eliminamos porque en el if principal (linea 130) no indica que hay jugadores (length > 0)
            if (info != undefined){
                const padre = info.parentNode;
                padre.removeChild(info);
            } 
            

            carousel.appendChild(DOM.createElement('img', undefined, ['carousel__profile'], [{name:'alt', value: players.data[posicion].last_name}]));
            var img = document.getElementsByClassName('carousel__profile')[0];
            
            const src = `/assets/${players.data[posicion].id}.jpg`;
            img.setAttribute('src', src);
            // Con este onerror captamos si la dirección de la img (src) no existe, para que cargue una imagen por defecto, ya que no todos los jugadores tienen imagen
            img.onerror = () => { img.setAttribute('src', '/assets/default.jpg'); } // Verifica si se ha producido un error al cargar la imagen, si es así, carga la imagen por default
            
            img.setAttribute('alt', players.data[posicion].last_name);

            // Container para almacenar los datos del jugador
            const carouselData = DOM.createElement('div', undefined, ['carousel__data']);
            carousel.appendChild(carouselData);
            
            carouselData.appendChild(DOM.createElement('p', `Nombre: ${players.data[posicion].first_name}`, ['carousel__data__p']));
            carouselData.appendChild(DOM.createElement('p', `Apellido: ${players.data[posicion].last_name}`, ['carousel__data__p']));
            carouselData.appendChild(DOM.createElement('p', `Posición: ${players.data[posicion].position}`, ['carousel__data__p']));
            carouselData.appendChild(DOM.createElement('p', `Equipo: ${players.data[posicion].team.full_name}`, ['carousel__data__p']));
        } else {
            // No hay jugadores para la búsqueda
            const sectionPlayers = document.getElementsByClassName('section__players')[0];
            var carousel = document.getElementsByClassName('carousel')[0];

            if (carousel != undefined){
                // Si el div carousel existe, lo elimina, y crea un párrafo para indicar que no hay jugadores.
                const padre = carousel.parentNode;
                padre.removeChild(carousel);
                sectionPlayers.appendChild(DOM.createElement('p', "No hay jugadores...", ['carousel__data__info']));
            } 
        }
    }


    /**
     * Crea el contenedor donde se muestran los resultados de los juegos
     * @param {int} numberGames 
     * @param {array} games 
     */
    static createGames(numberGames, games){
        var sectionGames = document.getElementsByClassName('section__games')[0];
        var allGames = document.getElementById('allGames');
        
        if (allGames != undefined){
            const padre = allGames.parentNode;
            padre.removeChild(allGames);
            allGames = DOM.createElement('div', undefined, undefined, [{name: 'id', value:'allGames'}]);
            sectionGames.appendChild(allGames);
        } else {
            allGames = DOM.createElement('div', undefined, undefined, [{name: 'id', value:'allGames'}]);
            sectionGames.appendChild(allGames);
        }

        for (let i=0; i<numberGames; i++){
            const id =  games.data[i].id.toString();
            allGames.appendChild(DOM.createElement('div', undefined,['games'], [{name:'id', value: `games${id}`}]));
            const gameDiv = document.getElementById(`games${id}`);

            // Un div para las fechas y otro para los datos de los equipos (nombre, score)
            gameDiv.appendChild(DOM.createElement('div', undefined, ['games__container'], [{name:'id', value: `container1_${id}`}]));
            gameDiv.appendChild(DOM.createElement('div', undefined, ['games__container'], [{name:'id', value: `container2_${id}`}]));

            // Fecha del juego
            const containerDate = document.getElementById(`container1_${id}`);
            containerDate.appendChild(DOM.createElement('p', games.data[i].date.substring(0,10), ['games__container__date']));

            // Equipos
            const containerTeam = document.getElementById(`container2_${id}`);
            containerTeam.appendChild(DOM.createElement('div', undefined, ['games_container__team'], [{name:'id', value: `localTeam_${id}`}]));
            containerTeam.appendChild(DOM.createElement('div', undefined, ['games_container__team'], [{name:'id', value: `visitorTeam_${id}`}]));

            const localTeam = document.getElementById(`localTeam_${id}`);
            localTeam.appendChild(DOM.createElement('p', ` ${games.data[i].home_team.full_name} (${games.data[i].home_team.abbreviation})`));
            localTeam.appendChild(DOM.createElement('p', ` Score: (${games.data[i].home_team_score})`));

            const visitTeam = document.getElementById(`visitorTeam_${id}`);
            visitTeam.appendChild(DOM.createElement('p', ` ${games.data[i].visitor_team.full_name} (${games.data[i].visitor_team.abbreviation})`));
            visitTeam.appendChild(DOM.createElement('p', ` Score: (${games.data[i].visitor_team_score})`));
        }
    }
}



